import java.util.Arrays;

/**
 * 给定两个数组 arr1 和 arr2 ，其中数组 arr1 的长度大于等于数组 arr2 的长度，请你将数组 arr2 中的数据放入数组 arr1 的后面（替换）。
 * 当数组 arr1 的值为 [1, 2, 3, 4, 5, 6] ，数组 arr2 的值为 [7, 8, 9] 时，输出结果应该为:[1, 2, 3, 7, 8, 9]
 * 说明：
 * 数组 arr1 中最后三个数据 [7, 8, 9] 被数组 arr2 中的数据替换掉。
 * 所以最终的输出结果为 arr1=[1, 2, 3, 7, 8, 9]。
 * 哈哈哈哈，在测试啦。。。。。。
 */

public class Test01 {
    public static void main(String[] args){
        int[] arr1 = {1123 , 1123 , 3123 , 61233 , 7123 , 923 , 12 , 234 , 2342 , 2 , 4 , 34 , 34 , 354 , 7 , 8};
        int[] arr2 = {123 , 123 , 435 , 56 , 4 , 34 , 354 , 34 ,987};
        Test01 test01 = new Test01();
        int[] ints = test01.replaceArray(arr1, arr2);
        System.out.println(Arrays.toString(ints));
        //System.out.println(Arrays.toString(test01.replaceArray(arr1 , arr2)));


    }

    public int[]  replaceArray(int[] arr1 , int[] arr2){
        int a = arr1.length;
        int b = arr2.length;
        int c = a - b;
        int[] arr = new int[a];
        for(int i = 0 ; i < c ; i++){
            arr[i] = arr1[i];
        }
        for(int i = c ; i < a ; i++){
            arr[i] = arr2[i - c];
        }

        return arr;
    }
}
